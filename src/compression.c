/*
 * compression.c
 *
 *  Created on: 27 May 2018
 *      Author: Skyper
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "compression.h"


static dtSingCounter TableOfFreaqency[256];
static size_t TotalSingsInInputFile;
static struct dtTreeNode HuffmanTree[512];
static size_t HuffmanTreeIndx;
static dtCodedSing EncodingTable[256];
static unsigned int EncodingTableIndx;
static dtCodedSing UnsortedEncodingTable[256];


static inline void createTableOfFreaqency(char *inputFilePath);
static inline void sortTableOfFreaqency(void);
static inline void createHTree(void);
static inline void createEncodingTable(void);
static inline void encodeFile(char *inputFilePath, char *outputFilePath);
static inline int  cmpFuncDtSingCounter (const void * a, const void * b);
static inline void saveHeaderToOutputFile(FILE *pOutputFile);
static inline void saveEncodingTableToOutputFile(FILE *pOutputFile);
static inline void saveNumberOfSymbolsToOutputFile(FILE *pOutputFile);
static inline void saveEnderToOutputFile(FILE *pOutputFile);
static inline int  cmpFuncTreeNode (const void * a, const void * b);
static inline void getCodeFromTree(struct dtTreeNode* tree, char* code);
static inline int  cmpFuncCodedSing (const void * a, const void * b);
static inline void initTableOfFreaqency(void);
static inline  size_t copyStrToBitArray(char *bitCode, char *charCode);


void archive(char *inputFilePath, char *outputFilePath)
{
	initTableOfFreaqency();

	createTableOfFreaqency(inputFilePath);
	sortTableOfFreaqency();

	createHTree();
	createEncodingTable();

	encodeFile(inputFilePath, outputFilePath);
}

static inline void createTableOfFreaqency(char *inputFilePath)
{
	FILE *fp = fopen(inputFilePath, "rb");
	if(fp == NULL)
	{
		perror("ERROR:");
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(1);
	}

	long int inputFileSize = getFileSize(fp);

	unsigned char *r_buffer = (unsigned char*)malloc(sizeof(unsigned char) * inputFileSize);
	if (r_buffer == NULL)
	{
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(2);
	}

	while (!feof(fp))
	{
		size_t number = fread(r_buffer, sizeof(char), inputFileSize, fp);
		for (size_t i = 0; i < number; ++i)
		{
			++TableOfFreaqency[r_buffer[i]].counter;
			++TotalSingsInInputFile;
		}
	}

	free(r_buffer);
	fclose(fp);

	return;
}

static inline void sortTableOfFreaqency(void)
{
	qsort(TableOfFreaqency, 256, sizeof(dtSingCounter), cmpFuncDtSingCounter);
}

static inline void createHTree(void)
{
	unsigned int realSize = 0;

	for (int i = 0; i < 256; ++i)
	{
		if(TableOfFreaqency[i].counter > 0)
		{
			++realSize;
		}
	}

	struct dtTreeNode TableOfFreaqencyWOEmpty[256] = {0};

	for(unsigned int i = 0; i < realSize; ++i)
	{
		TableOfFreaqencyWOEmpty[i].value.counter = TableOfFreaqency[i + 256 - realSize].counter;
		TableOfFreaqencyWOEmpty[i].value.sing = TableOfFreaqency[i + 256 - realSize].sing;
	}

	unsigned int iterator = 0;
	while(iterator < (realSize - 1))
	{
		struct dtTreeNode min1, min2, new;

		min1 = TableOfFreaqencyWOEmpty[iterator];
		min2 = TableOfFreaqencyWOEmpty[iterator + 1];

		memset(TableOfFreaqencyWOEmpty + iterator, 0, 2 * sizeof(struct dtTreeNode));

		HuffmanTree[HuffmanTreeIndx] = min1;
		new.left = &HuffmanTree[HuffmanTreeIndx];
		++HuffmanTreeIndx;

		HuffmanTree[HuffmanTreeIndx] = min2;
		new.right = &HuffmanTree[HuffmanTreeIndx];
		++HuffmanTreeIndx;

		new.value.counter = min1.value.counter + min2.value.counter;
		TableOfFreaqencyWOEmpty[iterator + 1] = new;

		++iterator;

		qsort(TableOfFreaqencyWOEmpty + iterator, realSize - iterator, sizeof(struct dtTreeNode), cmpFuncTreeNode);
	}

	HuffmanTree[HuffmanTreeIndx] = TableOfFreaqencyWOEmpty[iterator];

	/* Move head to [511] */
	struct dtTreeNode Temp = HuffmanTree[HuffmanTreeIndx];
	HuffmanTree[HuffmanTreeIndx] = HuffmanTree[511];
	HuffmanTree[511] = Temp;
	HuffmanTreeIndx = 511;
}

static inline void createEncodingTable(void)
{
	char code[256] = {""};
	getCodeFromTree(&HuffmanTree[HuffmanTreeIndx], code);

	for(unsigned int i = 0; i < EncodingTableIndx; ++i)
	{
		memcpy(&UnsortedEncodingTable[EncodingTable[i].sing], &EncodingTable[i], sizeof(dtCodedSing));
	}

	qsort(EncodingTable, EncodingTableIndx, sizeof(dtCodedSing), cmpFuncCodedSing);
}

static inline void encodeFile(char *inputFilePath, char *outputFilePath)
{
	FILE *pInputFile = openInputFile(inputFilePath);
	FILE *pOutputFile = openOutputFile(outputFilePath);

	long int inputFileSize = getFileSize(pInputFile);

	unsigned char *r_buffer = (unsigned char*)malloc(sizeof(unsigned char) * inputFileSize);
	if (r_buffer == NULL)
	{
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(2);
	}

	unsigned char *w_buffer = (unsigned char*)calloc(WRITE_BUF_SIZE, sizeof(unsigned char));
	if (w_buffer == NULL)
	{
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(2);
	}

	saveHeaderToOutputFile(pOutputFile);
	saveNumberOfSymbolsToOutputFile(pOutputFile);
	saveEncodingTableToOutputFile(pOutputFile);
	saveEnderToOutputFile(pOutputFile);

	size_t usedSizeWBuffer = 0;
	while (!feof(pInputFile))
	{
		size_t number = fread(r_buffer, sizeof(char), inputFileSize, pInputFile);

		for (size_t i = 0; i < number; ++i)
		{
			/* Find code */
			bitarray_copy(&(UnsortedEncodingTable[r_buffer[i]].bitCode), 0, UnsortedEncodingTable[r_buffer[i]].bitCodeLength, w_buffer, usedSizeWBuffer);
			usedSizeWBuffer += UnsortedEncodingTable[r_buffer[i]].bitCodeLength;

			if(usedSizeWBuffer >= (WRITE_BUF_SIZE - 8) * 8)
			{
				fwrite(w_buffer, sizeof(unsigned char), (usedSizeWBuffer / 8), pOutputFile);
				w_buffer[0] = w_buffer[usedSizeWBuffer / 8];
				usedSizeWBuffer %= 8;
			}
		}
	}

	if(usedSizeWBuffer > 0)
	{
		fwrite(w_buffer, sizeof(unsigned char), usedSizeWBuffer / 8, pOutputFile);
		if((usedSizeWBuffer % 8) != 0)
		{
			fwrite(w_buffer + (usedSizeWBuffer / 8), sizeof(unsigned char), 1, pOutputFile);
		}
		usedSizeWBuffer = 0;
	}

	free(r_buffer);
	free(w_buffer);

	fclose(pInputFile);
	fclose(pOutputFile);
}

static inline int cmpFuncDtSingCounter (const void * a, const void * b)
{
	dtSingCounter *firstValue = (dtSingCounter *)a;
	dtSingCounter *secondValue = (dtSingCounter *)b;

	return ((*firstValue).counter - (*secondValue).counter);
}

static inline void saveHeaderToOutputFile(FILE *pOutputFile)
{
	/* Save encoding table to output file */
	char header[] = "Start encoding table.\n";
	fwrite(header, sizeof(char), strlen(header), pOutputFile);
}

static inline void saveEncodingTableToOutputFile(FILE *pOutputFile)
{
	fwrite(HuffmanTree, sizeof(char), sizeof(HuffmanTree), pOutputFile);
}

void saveNumberOfSymbolsToOutputFile(FILE *pOutputFile)
{
	char numberOfSymbols[256] = {'\0'};
	sprintf (numberOfSymbols, "Number of a symbols: %d\n", TotalSingsInInputFile);
	fwrite(numberOfSymbols, sizeof(char), strlen(numberOfSymbols), pOutputFile);
}

static inline void saveEnderToOutputFile(FILE *pOutputFile)
{
	char ender[] = "\nFinish encoding table.\n";
	fwrite(ender, sizeof(char), strlen(ender), pOutputFile);
}

int cmpFuncTreeNode (const void * a, const void * b)
{
	struct dtTreeNode *firstValue = (struct dtTreeNode *)a;
	struct dtTreeNode *secondValue = (struct dtTreeNode *)b;

	return ((*firstValue).value.counter - (*secondValue).value.counter);
}

static inline void getCodeFromTree(struct dtTreeNode* tree, char* code)
{
	if((tree->left == NULL) && (tree->right == NULL))
	{
		for (int i = 0; i < 256; ++i)
		{
			if(tree->value.sing == TableOfFreaqency[i].sing)
			{
				EncodingTable[EncodingTableIndx].sing = tree->value.sing;
				EncodingTable[EncodingTableIndx].counter = tree->value.counter;
				EncodingTable[EncodingTableIndx].bitCodeLength = copyStrToBitArray(&(EncodingTable[EncodingTableIndx].bitCode), code);
				++EncodingTableIndx;

				/*delete last symbol*/
				code[strlen(code) - 1] = '\0';
				return;
			}
		}
	}
	/*add '0' symbol*/
	strcat(code, "0");
	getCodeFromTree(tree->left, code);

	/*add '1' symbol*/
	strcat(code, "1");
	getCodeFromTree(tree->right, code);

	/*delete last symbol*/
	code[strlen(code) - 1] = '\0';
}

static inline int cmpFuncCodedSing (const void *a, const void *b)
{
	dtCodedSing *firstValue = (dtCodedSing *)b;
	dtCodedSing *secondValue = (dtCodedSing *)a;

	return ((*firstValue).counter - (*secondValue).counter);
}

static inline void initTableOfFreaqency(void)
{
	for (int i = 0; i < 256; ++i)
	{
		TableOfFreaqency[i].sing = (unsigned char)i;
	}
}

static inline size_t copyStrToBitArray(char *bitCode, char *charCode)
{
	size_t result = strlen(charCode);

	for(size_t i = 0; i < result; ++i)
	{
		if(charCode[i] == '1')
		{
			bitCode[i / 8] |= 1 << (7 - (i % 8));
		}
	}

	return result;
}
