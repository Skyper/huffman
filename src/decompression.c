/*
 * decompression.c
 *
 *  Created on: 27 May 2018
 *      Author: Skyper
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "main.h"
#include "decompression.h"


static struct dtTreeNode ReadHuffmanTree[512];
static dtCodedSing UnsortedEncodingTable2[256];
static dtCodedSing EncodingTable2[256];


static inline uint64_t readEncodingTable(char *inputFilePath);
static inline uint64_t readNumberSymbols(FILE *pInputFile);
static inline void dencodeFile(char *inputFilePath, char *outputFilePath, uint64_t numberSymbols);
static inline void readHeader(FILE *pInputFile);
static inline int findCodeInTree(struct dtTreeNode *tree, uint8_t *input, uint8_t offest, uint8_t *sing);


void unarchive(char *inputFilePath, char *outputFilePath)
{
	uint64_t numberSymbols = readEncodingTable(inputFilePath);
	dencodeFile(inputFilePath, outputFilePath, numberSymbols);
}

uint64_t readEncodingTable(char *inputFilePath)
{
	FILE *pInputFile = fopen(inputFilePath, "rb");
	if(pInputFile == NULL)
	{
		perror("ERROR:");
		printf("ERROR: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(1);
	}

	readHeader(pInputFile);
	uint64_t numberSymbols = readNumberSymbols(pInputFile);

	fread(ReadHuffmanTree, sizeof(char), sizeof(ReadHuffmanTree), pInputFile);

	fclose(pInputFile);

	for(unsigned int i = 0; i < 256; ++i)
	{
		memcpy(&UnsortedEncodingTable2[EncodingTable2[i].sing], &EncodingTable2[i], sizeof(dtCodedSing));
	}

	return numberSymbols;
}

void dencodeFile(char *inputFilePath, char *outputFilePath, uint64_t numberSymbols)
{
	FILE *pInputFile = openInputFile(inputFilePath);
	FILE *pOutputFile = openOutputFile(outputFilePath);

	long int inputFileSize = getFileSize(pInputFile);

	unsigned char *r_buffer = (unsigned char*)malloc(sizeof(unsigned char) * inputFileSize);
	if (r_buffer == NULL)
	{
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(2);
	}

	unsigned char *w_buffer = (unsigned char*)calloc(WRITE_BUF_SIZE, sizeof(unsigned char));
	if (w_buffer == NULL)
	{
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(2);
	}

	char string[512];
	char ender[] = "Finish encoding table.\n";
	do{
		if(fgets(string, 512, pInputFile) == NULL)
		{
			printf("ERROR: file: %s, line: %d\n", __FILE__, __LINE__);
			exit(1);
		}
	}while(strcmp(string, ender) != 0);

	uint64_t numberReadSumbols = 0;
	uint64_t w_buffer_indx = 0;
	uint8_t offset = 0;
	uint64_t i = 0;

	fread(r_buffer, sizeof(char), inputFileSize, pInputFile);
	while(numberReadSumbols < numberSymbols)
	{
		i += findCodeInTree(&ReadHuffmanTree[511], r_buffer + (i / 8), offset, w_buffer + w_buffer_indx);
		offset =  i % 8;
		++w_buffer_indx;
		++numberReadSumbols;

		if(w_buffer_indx >= WRITE_BUF_SIZE)
		{
			fwrite(w_buffer, sizeof(char), w_buffer_indx, pOutputFile);
			w_buffer_indx = 0;
		}
	}

	if(w_buffer_indx > 0)
	{
		fwrite(w_buffer, sizeof(char), w_buffer_indx, pOutputFile);
	}

	free(w_buffer);
	free(r_buffer);

	fclose(pInputFile);
	fclose(pOutputFile);
}

void readHeader(FILE *pInputFile)
{
	char string[512] = {0};
	char header[] = "Start encoding table.\n";

	fread(string, sizeof(char), strlen(header), pInputFile);

	if(strcmp(string, header) != 0)
	{
		printf("%s\n", string);
		printf("ERROR: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(3);
	}
}

uint64_t readNumberSymbols(FILE *pInputFile)
{
	uint64_t result;

	fscanf(pInputFile, "Number of a symbols: %"PRIu64"\n", &result);

	return result;
}

int findCodeInTree(struct dtTreeNode *tree, uint8_t *input, uint8_t offest, uint8_t *sing)
{
    int counter = 0;

    const struct dtTreeNode *node = tree;
    while (node->left != NULL)
    {
        uint8_t mask = 1 << (7 - offest);
        uint8_t bit = (input[0] & mask) != 0;

        node = bit ? node->right : node->left;

        ++counter;
        ++offest;
        if (offest > 7)
        {
        	offest = 0;
            ++input;
        }
    }

    *sing = node->value.sing;
    return counter;
}
