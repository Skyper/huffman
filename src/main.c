/*
 ============================================================================
 Name        : Huffman.c
 Author      : Petr Kizin
 Version     :
 Copyright   : _
 Description : Dumb implementation of the Huffman algorithm
 ============================================================================
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"
#include "compression.h"
#include "decompression.h"
#include "md5.h"


static inline void processInputs(int argc, char *argv[], char *inputFilePath,
				          char *outputFilePath, char *outputUnzippedFilePath,
						  char *operationType);


int main (int argc, char *argv[])
{
	char inputFilePath[256];
	char outputFilePath[256];
	char outputUnzippedFilePath[256];
	char operationType;

	clock_t start = clock();

	processInputs(argc, argv, inputFilePath, outputFilePath, outputUnzippedFilePath, &operationType);

	switch(operationType)
	{
		case 'c':
			archive(inputFilePath, outputFilePath);
			break;

		case 'd':
			unarchive(inputFilePath, outputFilePath);
			break;

		case 't':
			archive(inputFilePath, outputFilePath);
			printf("archive preparing: %lu ms\n", clock() - start);

			unarchive(outputFilePath, outputUnzippedFilePath);
			printf("archive: %lu ms\n", clock() - start);

			MD5CompareTwoFiles(inputFilePath, outputUnzippedFilePath);
			break;

		case 'm':
			MD5CompareTwoFiles(inputFilePath, outputFilePath);
			break;

		default:
			printf("Incorrect type operation\n");
			printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
			exit(3);
			break;
	}

	return EXIT_SUCCESS;
}

FILE *openInputFile(char *path)
{
	FILE *pInputFile = fopen(path, "rb");
	if(pInputFile == NULL)
	{
		perror("ERROR:");
		printf("ERROR: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(1);
	}

	return pInputFile;
}

FILE *openOutputFile(char *path)
{
	FILE *pOutputFile = fopen(path, "w+b");
	if(pOutputFile == NULL)
	{
		perror("ERROR:");
		printf("ERROR: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(1);
	}

	return pOutputFile;
}

long int getFileSize(FILE *stream)
{
	fseek(stream, 0, SEEK_END); // seek to end of file
	long int result = ftell(stream); // get current file pointer
	fseek(stream, 0, SEEK_SET); // seek back to beginning of file

	return result;
}

void processInputFileParamiters(char *argv[], char *inputFilePath)
{
	if(strcmp(argv[1], "-i") == 0)
	{
		printf("Input file: %s\n",argv[2]);
		strcpy(inputFilePath, argv[2]);
	}
	else
	{
		printf("Incorrect order of input parameters\n");
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(3);
	}
}

void processOutputFileParamiters(char *argv[], char *outputFilePath)
{
	if(strcmp(argv[3], "-o") == 0)
	{
		printf("Output file: %s\n",argv[4]);
		strcpy(outputFilePath, argv[4]);
	}
	else
	{
		printf("Incorrect order of input parameters\n");
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(3);
	}
}

void processOperationTypeParamiter(char *argv[],
		                           char *outputUnzippedFilePath,
		                           char *operationType)
{
	if(strcmp(argv[5], "-c") == 0)
	{
		*operationType = 'c';
		printf("Operation type: compression\n");
	}
	else if(strcmp(argv[5], "-d") == 0)
	{
		*operationType = 'd';
		printf("Operation type: decompression\n");
	}
	else if(strcmp(argv[5], "-cd") == 0)
	{
		*operationType = 't';
		strcpy(outputUnzippedFilePath, argv[4]);
		strcat(outputUnzippedFilePath, "_unzipped");
		printf("Operation type: testing (compression and decompression)\n");
		printf("Output unzipped file: %s\n", outputUnzippedFilePath);
	}
	else if(strcmp(argv[5], "-m") == 0)
	{
		*operationType = 'm';
		printf("Operation type: files comparing\n");
	}
	else
	{
		printf("Incorrect order of input parameters\n");
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(3);
	}
}

void processInputs(int argc,
		           char *argv[],
				   char *inputFilePath,
				   char *outputFilePath,
				   char *outputUnzippedFilePath,
				   char *operationType)
{
	if (argc < 6)
	{
		printf("Incorrect number of input parameters\n");
		printf("Error: file: %s, line: %d\n", __FILE__, __LINE__);
		exit(3);
	}

	processInputFileParamiters(argv, inputFilePath);
	processOutputFileParamiters(argv, outputFilePath);
	processOperationTypeParamiter(argv, outputUnzippedFilePath, operationType);
}
