/*
 * compression.c
 *
 *  Created on: 27 May 2018
 *      Author: Skyper
 */

#ifndef INCLUDE_MAIN_H_
#define INCLUDE_MAIN_H_

#define WRITE_BUF_SIZE	(10*1024*1024) /* 10Mb */

typedef struct
{
	unsigned char sing;
	char bitCode[8];
	size_t bitCodeLength;
	size_t counter;
} dtCodedSing;

typedef struct
{
	unsigned char sing;
	int counter;
} dtSingCounter;

struct dtTreeNode
{
	dtSingCounter value;
	struct dtTreeNode *left;
	struct dtTreeNode *right;
};

long int getFileSize(FILE *stream);
FILE *openInputFile(char *path);
FILE *openOutputFile(char *path);

#endif /* INCLUDE_MAIN_H_ */
