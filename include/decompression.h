/*
 * decompression.h
 *
 *  Created on: 27 May 2018
 *      Author: Skyper
 */

#ifndef INCLUDE_DECOMPRESSION_H_
#define INCLUDE_DECOMPRESSION_H_



void unarchive(char *inputFilePath, char *outputFilePath);

#endif /* INCLUDE_DECOMPRESSION_H_ */
