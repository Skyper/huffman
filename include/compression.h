/*
 * compression.c
 *
 *  Created on: 27 May 2018
 *      Author: Skyper
 */

#ifndef INCLUDE_COMPRESSION_H_
#define INCLUDE_COMPRESSION_H_


void archive(char *inputFilePath, char *outputFilePath);


#endif /* INCLUDE_COMPRESSION_H_ */
